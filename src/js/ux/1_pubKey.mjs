import {checkKey} from "../../../node_modules/g1lib/browser/crypto.mjs"
export function init(){
    const pubKeyInputNode = document.getElementById('pubKey');
    pubKeyInputNode.addEventListener('change',updatePubKey);
    pubKeyInputNode.addEventListener('keyup',updatePubKey);
}
init();
export default function focus_1_pubKey(){
  const toFocus = document.querySelector('#pubKey');
  toFocus.focus({preventScroll:true});
}


export let pubKey;
export function updatePubKey(){
    const pubKeyInputNode = document.getElementById('pubKey');
    if(location.hash.match(/pubkey=/i) !== null){
      pubKeyInputNode.value = location.hash.split('=')[1];
      return location.hash = location.hash.split('=')[0];
    }
    pubKey = pubKeyInputNode.value;
    const nextBtn = document.querySelector('.step_1_pubKey a.button');
    const errorArea = document.querySelector('.step_1_pubKey .warning');
    try {
        checkKey(pubKey)
        nextBtn.classList.remove('disabled');
        nextBtn.setAttribute('href','#page_2_inputSecrets');
        nextBtn.innerHTML = `Suivant <i class="fas fa-long-arrow-alt-right"/>`;
        errorArea.classList.add('hidden');
        pubKeyInputNode.parentNode.setAttribute('action','#page_2_inputSecrets');
    } catch (err){
        nextBtn.classList.add('disabled');
        nextBtn.removeAttribute('href');
        errorArea.classList.remove('hidden');
        pubKeyInputNode.parentNode.removeAttribute('action');
        let errorMessage;
        switch (err.name){
            case 'empty': errorMessage = `Aucune saisie.<br/><br/>Merci d'indiquer la clef publique du compte à récupérer.`;break;
            case 'too_short': errorMessage = `Clef trop courte.<br/><br/>Vérifiez que vous avez copié l'intégralité de votre clef publique.`;break;
            case 'too_long': errorMessage = `Clef trop longue.<br/><br/>Avez vous copié plus que votre clef publique ?`;break;
            case 'not_b58': errorMessage = `Caractère incompatible détecté.<br/><br/>Merci de saisir votre clef publique en base 58. Voici la liste des caractères compatibles :<br/>123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz`;break;
            case 'bad_ed25519_point': errorMessage = `Clef invalide.<br/><br/>Il est probable que vous ayez remplacé un caractère par un autre en recopiant. Vérifiez bien votre saisie.`;break;
            default:
              errorMessage = `Format incorrect, vérifiez votre saisie.\n<br/>${err}`;
              console.error(err);
              break;
        }
        const help = `<a href="javascript:alert('TODO:page assistance. En attendant : g1@1000i100.fr');">Besoin d'aide ?</a>`;
        errorArea.innerHTML = `<center>${errorMessage}<br/><br/>${help}</center>`;
    }
    return pubKey;
}
