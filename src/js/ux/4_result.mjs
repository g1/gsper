import {departTime, duplicateCount, endTime, found, tested} from "../logic/search.mjs";
import prettyMilliseconds from "pretty-ms";

async function select(id){
  const targetNode = document.getElementById(id);
  window.getSelection().selectAllChildren(targetNode);
  try{
    await navigator.clipboard.writeText(targetNode.innerText);
  }catch (err){
    //console.error(`Échec d'écriture dans le presse papier: `, err);
  }
}
document.getElementById('resIdSec').addEventListener('click',()=>{select('resIdSec');});
document.getElementById('resPass').addEventListener('click',()=>{select('resPass');});

export default function showResult(){
  if(found){
    document.querySelectorAll('.success').forEach(n=>n.classList.remove('hidden'));
    document.querySelectorAll('.notFound').forEach(n=>n.classList.add('hidden'));

    document.getElementById('resIdSec').innerHTML = spanEachChar(found.idSec);
    document.getElementById('resPass').innerHTML = spanEachChar(found.pass);
      } else {
    document.querySelectorAll('.notFound').forEach(n=>n.classList.remove('hidden'));
    document.querySelectorAll('.success').forEach(n=>n.classList.add('hidden'));
  }

  // stats
  if(!departTime) return;
  document.getElementById('resTested').innerHTML = tested;
  const timeSpent = endTime - departTime;
  document.getElementById('resSpent').innerHTML = timeFormat(timeSpent);
  document.getElementById('resSpeed').innerHTML = (1000*tested/timeSpent).toFixed(1);
  const duplicates = duplicateCount();
  if (duplicates) {
    document.getElementById('resDup').classList.remove('hidden');
    document.getElementById('resDuplicate').innerHTML = duplicates;
    document.getElementById('resDupRatio').innerHTML = (100 * duplicates / tested).toPrecision(3);
  } else {
    document.getElementById('resDup').classList.add('hidden');
  }
}
function timeFormat(milliseconds) {
  return prettyMilliseconds(Math.trunc(milliseconds / 1000)*1000, {
    verbose: true,
    upToUnit: 'By',
    pluralizeFunc: (n) => n > 1,
  });
}

