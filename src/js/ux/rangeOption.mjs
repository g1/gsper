
const hostNode = document.getElementById('optionsWidgetList');
export default function RangeOption(fieldId, initialValue, descArray, uiListener){
  const self = {};
  self.listeners={};

  const grpNode = document.createElement('fieldset');
  grpNode.id = `roGrp_${fieldId}`;
  grpNode.classList.add('rangeOptionGrp');

  const descLabel = document.createElement('label');
  descLabel.classList.add('dynDesc');
  descLabel.innerHTML = descArray[initialValue];
  grpNode.appendChild(descLabel);

  const hiddenNode = document.createElement('input');
  hiddenNode.setAttribute('type','hidden');
  hiddenNode.id = `ro_${fieldId}`;
  hiddenNode.value = initialValue;
  grpNode.appendChild(hiddenNode);

  const checkBoxNode = document.createElement('input');
  checkBoxNode.setAttribute('type','checkbox');
  checkBoxNode.id = `roSwitchAuto_${fieldId}`;
  checkBoxNode.classList.add('roSwitchAuto');
  checkBoxNode.value = '-1';
  grpNode.appendChild(checkBoxNode);

  const labelForAuto = document.createElement('label');
  labelForAuto.setAttribute('for',`roSwitchAuto_${fieldId}`);
  labelForAuto.classList.add('autoSwitchDesc');
  labelForAuto.innerHTML = 'auto';
  grpNode.appendChild(labelForAuto);

  const rangeNode = document.createElement('input');
  rangeNode.setAttribute('type','range');
  rangeNode.id = `roManualRangeInput_${fieldId}`;
  rangeNode.classList.add('roManualRangeInput');
  rangeNode.setAttribute('min','0');
  rangeNode.setAttribute('max',`${Object.keys(descArray).reduce((acc,cur) => Math.max(parseInt(cur),acc),0)}`);
  rangeNode.setAttribute('step','1');
  rangeNode.value = initialValue;
  grpNode.appendChild(rangeNode);

  hostNode.appendChild(grpNode);

  self.listen = (listenerName,callbackFunction)=>self.listeners[listenerName] = callbackFunction;
  self.setComputed = val=>{
    val = parseInt(val);
    descLabel.innerHTML = descArray[val];
    if(val<0) checkBoxNode.checked = true;
    else {
      rangeNode.value = val;
    }
  };
  self.setUserChoice = val=>{
    self.setComputed(val);
    val = parseInt(val);
    hiddenNode.value = val;
    self.value = val;
    if(val>=0) checkBoxNode.checked = false;
  }
  function uiChanged(){
    const mode = parseInt(hiddenNode.value);
    self.value = mode;
    descLabel.innerHTML = descArray[mode];
    if(mode>=0) checkBoxNode.checked = false;
    Object.keys(self.listeners).forEach(k=>self.listeners[k](mode));
  }
  checkBoxNode.addEventListener('change', ()=>uiChanged(hiddenNode.value = checkBoxNode.checked?-1:(rangeNode.value || 0)));
  rangeNode.addEventListener('input', ()=>descLabel.innerHTML = descArray[rangeNode.value]);
  rangeNode.addEventListener('change', ()=>uiChanged(hiddenNode.value = rangeNode.value));
  if(typeof uiListener !== 'undefined') self.listen('default',uiListener)
  if(initialValue) self.setUserChoice(initialValue);
  return self;
}
