import {startSearch, departTime, tested, searchRunning} from "../logic/search.mjs";
import prettyMilliseconds from "pretty-ms";
import getDico from "../logic/dico.mjs";
import {speedFormat} from "../logic/utils.mjs";


export default function init(){
  document.getElementById('startSearch').addEventListener('click', startSearch);
  setInterval(showProgress,200);
}
init();
const progressColor = getComputedStyle(document.querySelector('.success h3')).color;
const progressBar = document.getElementById('progress_bar');
const bgColor = getComputedStyle(progressBar).backgroundColor;
const timing = [];
const shortDelay = 10*1000;
const longTiming = [];
const longDelay = 60*1000;
function showProgress(){
  if(!searchRunning) {
    while(timing.length) timing.shift();
    while(longTiming.length) longTiming.shift();
    return;
  }
  const now = Date.now();
  timing.push({time:now,tested:tested});
  longTiming.push({time:now,tested:tested});
  while(timing[0].time < now-shortDelay) timing.shift();
  while(longTiming[0].time < now-longDelay) longTiming.shift();
  document.getElementById('timeSpent').innerHTML = timeFormat(now-departTime)
  const globalSpeed = tested/(now-departTime)*1000;
  document.getElementById('timeSpent').setAttribute('title', `${speedFormat(globalSpeed)}/s en moyenne`);
  document.getElementById('altTested').innerHTML = tested;
  const total = getDico().length;
  document.getElementById('altTotal').innerHTML = total;
  const altDiff = tested - timing[0].tested;
  const timeDiff = (now - timing[0].time) / 1000;
  const actualSpeed = altDiff/timeDiff;
  const longAltDiff = tested - longTiming[0].tested;
  const longTimeDiff = (now - longTiming[0].time) / 1000;
  const lastMinuteSpeed = longAltDiff/longTimeDiff;
  document.getElementById('searchSpeed').innerHTML = speedFormat(actualSpeed);
  document.getElementById('searchSpeed').setAttribute('title', `${speedFormat(lastMinuteSpeed)}/s sur la dernière minute écoulée`);
  const achievementPercent = (100*tested/total).toFixed(1);
  progressBar.style.backgroundImage = percentToGradient(achievementPercent);

  const estimateRemaining = 1 + (total-tested)/( (total-tested>10000) ? lastMinuteSpeed : actualSpeed );
  document.getElementById('timeRemaining').innerHTML = timeFormat(estimateRemaining*1000,true)
  document.getElementById('timeRemaining').setAttribute('title', timeFormat(estimateRemaining*1000));
}
function timeFormat(milliseconds, short=false) {
  const options = {
    verbose: true,
    upToUnit: 'By',
    pluralizeFunc: (n) => n > 1,
  };
  if(short) options.compact = true;
  return prettyMilliseconds(Math.trunc(milliseconds / 1000)*1000, options);
}
function percentToGradient(percent){
  return `linear-gradient(to right, ${progressColor} 0%, ${progressColor} ${percent}%, ${bgColor} ${percent}%, ${bgColor} 100%)`;
}
