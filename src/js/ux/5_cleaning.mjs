import {resetWorkers} from "../logic/workerManager.mjs";

export default function cleanAll(){
  localStorage.clear();
  document.getElementById('secrets').value = '';
  document.getElementById('pubKey').value = '';
  resetWorkers();
  window.location = './';
}
window.cleanAll = cleanAll;
