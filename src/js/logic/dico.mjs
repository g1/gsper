import {subscribe} from "./workerManager.mjs";
import getCombiPerSec from "./speedBench.mjs";
import {Dictionary} from "g1lib/browser/dictionary.mjs";
import staticPreview from "./staticPreview.mjs";
import {cacheScale} from "./search.mjs";

let currentDico;
let lastData;
let staticDico;
subscribe('smartPreview', setDico);
subscribe('fastPreview', setStaticDico);

export function genDico(){
  const data = getActualData();
  const dico = new Dictionary(data.secrets, data.options);
  data.dico = dico;
  staticPreview(data);
  data.dico = JSON.stringify(dico);
  setDico(data);
  currentDico = dico;
  return dico;
}
export function setDico(data){
  lastData = data;
  currentDico = undefined;
  staticDico = JSON.parse(data.dico);
}
function setStaticDico(data){
  staticDico = JSON.parse(data.dico);
}
export default function getDico(){
  if(!currentDico) {
    if(!lastData) throw new Error("Smart Dictionary not yet generated.");
    const dicoStruct = JSON.parse(lastData.dico);
    currentDico = new Dictionary(lastData.secrets, dicoStruct.config);
  }
  return currentDico;
}
export function getStaticDico(){
  if(staticDico) return staticDico;
  genDico();
  return getStaticDico();
}
window.getDico = getDico;
window.getStaticDico = getStaticDico;
export function getActualData(withSpeed = true) {
  const so = window.searchOptions;
  const result = {
    secrets: document.getElementById('secrets').value,
    options: {
      escapeAll:so.regex.value<0?undefined:so.regex.value===0?1:so.regex.value===1?0:so.regex.value,
      idSecPwd:so.mirror.value===0?0:1,
      accent:so.accent.value<0?undefined:so.accent.value,
      lowerCase:so.caps.value<0?undefined:so.caps.value,
      leetSpeak:0,
      save:so.save.value,
      cacheMax:cacheScale[so.cacheMax.value],
    },
    previewIndex: (window.browseIndex || 0),
  };
  if (withSpeed) try {
    result.options.speed = getCombiPerSec();
  } catch (e) {
    if (e.message !== 'Speed not yet estimated.') throw e;
  }
  return result;
}
