import {sleep} from "./utils.mjs";

const workers = [];
const MAX_ALLOWED_IN_QUEUE = 3;
const subscriptionList = {};

function startWorkers() {
  for (let i=0;i<navigator.hardwareConcurrency;i++){
    workers[i] = {queue:0,worker:new Worker('worker.js',{type:"module"})}; // en attendant un meilleur support des modules et import
    workers[i].worker.addEventListener('message', ev=>receivedFromWorker(ev,i));
  }
}
startWorkers();
export async function sendToWorker(action,data, workerID= -1){
  let succeed = false;
  while (!succeed){
    try{
      const targetWorker = chooseTargetWorker(workerID);
      if(!data && action.action) data = action;
      else data.action = action;
      targetWorker.worker.postMessage(data);
      targetWorker.queue++;
      succeed=true;
      await sleep(0);
    } catch (e){
      if (e.message === "Max queue size reached for all workers") {
        await sleep(100);
      } else throw e;
    }
  }
}
export function resetWorkers(){
  for (let i=0;i<navigator.hardwareConcurrency;i++){
    workers[i].worker.terminate();
    workers[i] = {queue:0,worker:new Worker('worker.js',{type:"module"})}; // en attendant un meilleur support des modules et import
    workers[i].worker.addEventListener('message', ev=>receivedFromWorker(ev,i));
  }
}
let loadBalancingCursor=0;
function chooseTargetWorker(workerID){
  if(workerID !== -1) return workers[workerID];
  const wLength = workers.length;
  for (let i =0 ; i<wLength ; i++){
    loadBalancingCursor = (loadBalancingCursor+1) % wLength;
    if(workers[loadBalancingCursor].queue<MAX_ALLOWED_IN_QUEUE) return workers[loadBalancingCursor];
  }
  throw new Error("Max queue size reached for all workers");
}
function receivedFromWorker(event,workerID){
  workers[workerID].queue--;
  if(event.data.action ==="loadBalance") workers[workerID].queue = event.data.queueSize;
  if(subscriptionList[event.data.action]) subscriptionList[event.data.action].forEach(func=>func(event.data));
}
export function subscribe(whatAction,callbackFunction){
  if(!subscriptionList[whatAction]) subscriptionList[whatAction] = [];
  subscriptionList[whatAction].push(callbackFunction);
}
export function unSubscribe(action,registeredFunc){
  const index = subscriptionList[action].indexOf(registeredFunc);
  subscriptionList[action].splice(index, 1);
}
