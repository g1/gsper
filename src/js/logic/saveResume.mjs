import {subscribe} from "./workerManager.mjs";
import {swapKeyValue,sleep} from "./utils.mjs";

export const saveScale = [];
saveScale[0]=0;
saveScale[1]=24*60*60;
saveScale[2]=2*24*60*60;
saveScale[3]=7*24*60*60;
export const revertSaveScale = swapKeyValue(saveScale);

const MIN_COMPUTE_TIME_FOR_SAVE = 3600;

export let saveMode=0

async function init(){
  await sleep(1);
  subscribe('smartPreview', updateSaveMode);
  window.searchOptions.save.listen('saveResume',manualUpdate);
}
init();
function manualUpdate(mode){
  if(mode>=0) saveMode = saveScale[mode];
}
function updateSaveMode(data){
  const smartDico = JSON.parse(data.dico);
  if(smartDico.config.save<0) saveMode = smartDico.estimateDuration >= MIN_COMPUTE_TIME_FOR_SAVE ? saveScale[2] : 0;
  else saveMode = saveScale[smartDico.config.save];
  window.searchOptions.save.setComputed(revertSaveScale[saveMode]);
}
